<?php
/**
 * **************************************************************************
 * 
 * File:         getServices.php
 *
 * Purpose:      Holds a simple example structure to guide developers to use 
 * 				 Text2Pay API library, using getServices action.
 *
 * Last 
 *   Changes:    Rev $Rev: 7 $
 *               on $Date: 2012-11-13 13:31:31 +1000 (Tue, 13 Nov 2012) $
 *               by $Author: bruno.braga@gmail.com $
 *
 * **************************************************************************
 * 
 * Copyright 2012 Text2Pay, Inc. http://www.text2pay.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, softwareAPI
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * 
 * ************************************************************************** 
 */

require_once (dirname(__FILE__) . '/../text2pay/api.php');

// your credentials here
$apiKey = '';
$pubId = '';
$username = '';
$password = '';

try {
    // Create a Text2Pay API connection
    $cn = new Text2PayApi_Connection($apiKey, $pubId, $username, $password);
    
    // Get List of Carriers
    $campaignId = 3308;
    $countryCode = 'CA';
    $o = new Text2PayApi_getServices_Request($cn, $campaignId, $countryCode);
    $services = $o->execute();

    // print results
    echo var_dump($services);

}
catch (Text2PayApiException $taex) {
    // This will hold most exceptions thrown by the API wrapper, usually 
    // related to user credentials or internet connectivity issues.
    echo '(' . $taex->getCode() . ') '. $taex->getMessage() . "\n";
}
catch (Text2PayException $tex) {
    // This is usually errors caused by developers, passing wrong info 
    // to the API wrapper.    
    echo '(' . $tex->getCode() . ') '. $tex->getMessage() . "\n";
}
catch (Exception $ex) {
    // Unknown errors should not happen, unless there are PHP issues 
    // (invalid libraries, etc)
    // Any troubleshooting here needs to be carefully analysed, and 
    // a support ticket may be requested to Text2Pay. 
    echo 'Should never happen! ' . $ex . "\n";
}
?>